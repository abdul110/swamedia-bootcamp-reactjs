import {Outlet, Link} from "react-router-dom";
import Header from '../Layouts/Header';

const MainLayout = () => {
  const routes = [
    {name: "Home", path: "/"},
    {name: "Class Example", path:"/class-example"},
    {name: "Hooks Example", path:"/hooks-example"},
    {name: "CRUD", children: [
      {name: "Crud With Hooks", path:"/crud-with-hooks"},
      {name: "Crud With Axios", path:"/crud-with-axios"},
      {name: "Crud With Ref", path:"/crud-with-ref"},
    ]},
    {name: "Movie", path:"/movie"},
    {name: "Contestant", path:"/contestants"}, 
    {name: "Footballers", path:"/footballers", privatePath: true},
    {name: "Contestant With Auth", path:"/contestants-with-auth", privatePath: true},
    {name: "Register", path:"/register", loginPath: true},
    {name: "Login", path:"/login", loginPath: true},
  ] 

  return (
    <div className="App">
      <Header items={routes} image="https://swamedia.co.id/wp-content/uploads/2020/08/logo_300.png"/>
      <div style={{width: "80%", margin: "auto", border: "1px solid #eee", padding: "20px", paddingTop: "50px"}}>

        <Outlet />

      </div>

      <div className="footer">
        <p>Swamedia ReactJS 2023</p>
      </div>
    </div>
  );
}

export default MainLayout
