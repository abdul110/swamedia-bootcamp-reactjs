import { signOut } from "firebase/auth";
import { useContext, useState } from "react";
import { Link } from "react-router-dom"
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from 'reactstrap';
import { UserContext } from "../Context/UserContext";
import { auth } from "../firebase";
const Header = (props)=>{
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useContext(UserContext)

  const logout = async ()=>{
    if (user.authType === "firebase"){
      await signOut(auth)
    }
    setUser(null)
    localStorage.removeItem("user")
  }

  const toggle = () => setIsOpen(!isOpen);
  return (
    <div>
      <Navbar
          color="light"
          light
          expand="sm"
        >
          <NavbarBrand href="/">
            <img style={{width: "120px"}} src={props.image}/>            
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ms-auto" navbar>
              {props.items.map((item, index)=>{
                let showMenu = true
                if (user){
                  showMenu = item.loginPath ? false : true
                }else{
                  showMenu = item.privatePath ? false : true
                }

                if (showMenu){
                  if (item.children){
                    return(
                      <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                          {item.name}
                        </DropdownToggle>
                        <DropdownMenu right>
                          {item.children.map((submenu, index)=>{
                            return(
                              <>
                                <DropdownItem> <Link className="no-link" to={submenu.path}> {submenu.name}</Link></DropdownItem>
                                {index < item.children.length - 1 &&  <DropdownItem divider />}
                              </>
                            )
                          })}
                          
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    )
                  }else{
                    return(
                      <NavItem key={index}>              
                        <NavLink>
                          <Link className="no-link" to={item.path}>{item.name}</Link>
                        </NavLink>
                      </NavItem>
                    )
                  }
                }
              })}
              {user && 
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    {user.name ? user.name : user.email}
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>{user.name ? user.name : user.email}</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem onClick={logout}>Logout</DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              }
              <div style={{paddingRight: "20px"}}></div>
            </Nav>
          </Collapse>
      </Navbar>
    </div>
  )
}

// function Header(props){
//   return(
//     <nav className='custom-nav'>
//       <ul>
//         <li><img src={props.image}/></li>
//         {props.items.map((item, index)=>{
//           return(
//             <li key={index}>              
//               <Link to={item.path}>{item.name}</Link>
//             </li>
//           )
//         })}
//       </ul>
//     </nav>
//   )
// }

export default Header