import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import MainRouter from './Router';
import { UserProvider } from './Context/UserContext';
function App() {
  return(
    <>
      <UserProvider>
        <MainRouter/>
      </UserProvider>
    </>
  )
}


export default App;
