import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom"
import NoMatch from "../Components/NoMatch"
import MainLayout from "../Layouts/MainLayout"
import Content from '../Components/Content';
import Contestants from '../Components/Contestants';
import ContestantsWithAuth from '../Components/ContestantsWithAuth';
import CRUDWithRef from '../Components/CRUDWithRef';
import Movie from '../Components/Movie';
import CRUDWithAxios from '../Components/CRUDWithAxios';
import CRUDWithHooks from '../Components/CRUDWithHooks';
import ClassExample from '../Components/ClassExample';
import HooksExample from '../Components/HooksExample';
import ContestantShow from "../Components/Contestants/Show";
import FootballerForm from "../Components/Footballers/Form";
import Footballers from "../Components/Footballers/Table";
import FootballerShow from "../Components/Footballers/Show";
import Home from "../Components/Home";
import Register from "../Components/Auth/Register";
import FirebaseRegister from "../Components/FirebaseAuth/Register";
import FirebaseLogin from "../Components/FirebaseAuth/Login";
import Login from "../Components/Auth/Login";
import { useContext } from "react";
import { UserContext } from "../Context/UserContext";



const MainRouter = () =>{
  const [user] = useContext(UserContext)

  const LoginRoute = ({children})=>{
    if (user){
      return <Navigate to="/" replace={true}/>
    }else{
      return children
    }
  }

  const PrivateRoute = ({children})=>{
    if (user){
      return children
    }else{
      return <Navigate to="/login" replace={true}/>
    }
  }

  return(
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route index element={<Home/>} />
            <Route path="/content-example" element={<Content times={10}/>} />
            <Route path="/class-example" element={<ClassExample start={20}/>} />
            <Route path="/hooks-example" element={<HooksExample/>} />
            <Route path="/crud-with-hooks" element={<CRUDWithHooks/>} />
            <Route path="/crud-with-axios" element={<CRUDWithAxios/>} />
            <Route path="/crud-with-ref" element={<CRUDWithRef/>} />
            <Route path="/movie" element={<Movie/>} />
            <Route path="/contestants" element={<Contestants/>} />
            <Route path="/contestants-with-auth" element={<PrivateRoute><ContestantsWithAuth/></PrivateRoute>} />
            <Route path="/contestants/:id" element={<ContestantShow/>} />     
            <Route path="/register" element={<LoginRoute><Register/></LoginRoute>} />      
            <Route path="/firebase-register" element={<LoginRoute><FirebaseRegister/></LoginRoute>} />      
            <Route path="/firebase-login" element={<LoginRoute><FirebaseLogin/></LoginRoute>} />      
            <Route path="/login" element={<LoginRoute><Login/></LoginRoute>} />
            <Route path="/footballers/create" element={<PrivateRoute><FootballerForm/></PrivateRoute>} />
            <Route path="/footballers" element={<PrivateRoute><Footballers/></PrivateRoute>} />
            <Route path="/footballers/:id" element={<PrivateRoute><FootballerShow/></PrivateRoute>} />
            <Route path="/footballers/:id/edit" element={<PrivateRoute><FootballerForm/></PrivateRoute>} />
     
            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </BrowserRouter>
  )
}

export default MainRouter