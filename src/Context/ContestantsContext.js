import React, { useState, createContext } from "react";

export const ContestantsContext = createContext();

export const ContestantsProvider = props => {
  const [contestants, setContestants] = useState(null);
  const [showForm, setShowForm] = useState(false)

  return (
    <ContestantsContext.Provider value={{contestants, setContestants, showForm, setShowForm}}>
      {props.children}
    </ContestantsContext.Provider>
  );
};