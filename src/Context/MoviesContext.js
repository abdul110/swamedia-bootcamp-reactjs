import React, { useState, createContext } from "react";

export const MoviesContext = createContext();

export const MoviesProvider = props => {
  const [movies, setMovies] = useState([
    { name: "Harry Potter", durationInMinutes: 120},
    { name: "Sherlock Holmes", durationInMinutes: 125},
    { name: "Avengers", durationInMinutes: 130},
    { name: "Spiderman", durationInMinutes: 124},
  ]);

  return (
    <MoviesContext.Provider value={[movies, setMovies]}>
      {props.children}
    </MoviesContext.Provider>
  );
};