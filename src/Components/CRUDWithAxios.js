import { useEffect, useState } from "react"
import axios from "axios"

const CRUDWithAxios = ()=>{
  const initialForm = {name: "", gender: "Laki-laki", height: ""}
  const [contestants, setContestants] = useState(null)
  const [showForm, setShowForm] = useState(false)
  const [input, setInput] = useState(initialForm)
  const [currentId, setCurrentId] = useState(null) // create state


  const fetchData = async ()=>{
    let result = await axios.get('https://super-bootcamp-backend.sanbersy.com/api/data-contestants')
    setContestants(result.data)
  }

  const handleChange = (event)=>{
    let value = event.target.value
    let name = event.target.name

    setInput({...input, [name]: value})

  }

  const handleSubmit = async (event)=>{
    event.preventDefault()
    if (currentId){
      await axios.put(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${currentId}`, input)
    }else{
      await axios.post('https://super-bootcamp-backend.sanbersy.com/api/data-contestants', input)
    }
    setContestants(null)
    setInput(initialForm)
    setShowForm(false)
    setCurrentId(null)
  }

  const backToTable = ()=>{
    setShowForm(false)
  }

  const handleEdit = async (id)=>{
    const result =  await axios.get(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${id}`)
    const {name, height, gender} = result.data

    setInput({name, height, gender})
    setCurrentId(id)
    setShowForm(true)
  }

  const handleDelete = async (id)=>{
    await axios.delete(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${id}`)

    setContestants(null)
    setCurrentId(null)
    setInput(initialForm)
  }


  const addNewContestant = ()=>{
    setShowForm(true)
  }

  useEffect(()=>{
    if (contestants === null){
      fetchData()
    }
  },[contestants])
  return(
    <>
      { showForm ? 
        <div className="custom-form-section">
          <form className="custom-form" onSubmit={handleSubmit}>
            <label htmlFor="name">Name</label>
            <input required onChange={handleChange} value={input.name} type="text" placeholder="Contestant name.." name="name"/>
            <label htmlFor="gender">Gender</label>
            <select required onChange={handleChange} value={input.gender} placeholder="Contestant gender.." name="gender">
              <option value="Laki-Laki">Laki Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
            <label htmlFor="height">Height</label>
            <input required onChange={handleChange} value={input.height} type="number" min={1} max={250} placeholder="Contestant height.." name="height"/>      
            <input type="submit" value={currentId ? "Update": "Submit"}/>
            <button onClick={backToTable} className="custom-button button-primary">
              Back
            </button>
          </form>
        </div> :
        <div style={{textAlign: "left"}} >
          <button onClick={addNewContestant} className="custom-button button-success">
            Add New Contestant
          </button>
        </div>
      }

      {
       !showForm && (
          <table className="custom-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {contestants && contestants.map((item, index)=>{
                return (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>
                      <button onClick={()=>{ handleEdit(item.id)}} className="custom-button button-primary">Edit</button>
                      <button onClick={()=>{ handleDelete(item.id)}} className="custom-button button-danger">Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
     )}

    </>
  )
}

export default CRUDWithAxios