import { useState } from "react"
import { Button, Col, Form, FormGroup, Input, Label, Table } from "reactstrap"

const CRUDWithHooks = ()=>{
  const [footballers, setFootballers] = useState([
    "Mbappe", "Sane", "Haaland", "Enzo", "Oshimen"
  ])
  const [input, setInput] = useState("")
  const [currentIndex, setCurrentIndex] = useState(-1) // create state
  const [showForm, setShowForm] = useState(false)

  const handleSubmit = (event)=>{
    event.preventDefault()
    if (currentIndex >=0){
      let newFootballers = footballers.map((item, index)=> {
        if (index === currentIndex){
          item = input
        }
        return item
      })
      setFootballers(newFootballers)
    }else{
      setFootballers([...footballers, input])
    }
    setInput("")
    setCurrentIndex(-1)
    setShowForm(false)
  }

  const handleChange = (event)=>{
    let value = event.target.value

    setInput(value)
  }

  const handleEdit = (index)=>{
    let footballer = footballers.find((item,idx)=>idx === index)

    setInput(footballer)
    setCurrentIndex(index)
    setShowForm(true)
  }

  const handleDelete = (index)=>{
    let newFootballers = footballers.filter((item,idx)=>idx !== index )
    setFootballers(newFootballers)
    setCurrentIndex(-1)
    setInput("")
  }

  const addNewFootballer = ()=>{
    setShowForm(true)
  }

  const backToTable = ()=>{
    setShowForm(false);
    setCurrentIndex(-1);
    setInput("")
  }

  return (
    <>
      { showForm ? 
        <div className="custom-form-section">
          <Form onSubmit={handleSubmit}>
            <FormGroup row>
              <Label
                for="footballerName"
                sm={2}
              >
                Name
              </Label>
              <Col sm={10}>
                <Input
                  value={input}
                  onChange={handleChange}
                  id="footballerName"
                  name="name"
                  placeholder="Footballer name.."
                  type="name"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={{offset: 2, size: 1}}>
                <Button color="success">{currentIndex >= 0 ? "Update": "Submit"}</Button>
              </Col>
              <Col sm={2}>
                <Button outline onClick={backToTable}>
                  Back
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </div> :
        <div style={{textAlign: "left"}} >
          <Button color="success" onClick={addNewFootballer}>
            Add New Footballer
          </Button>
        </div>
      }

      { !showForm && <Table>
        <thead>
          <tr>
            <th>No</th>
            <th style={{textAlign: "left"}}>Name</th>
            <th style={{textAlign: "left"}}>Action</th>
          </tr>
        </thead>
        <tbody>
          {footballers.map((item, index)=>{
            return (
              <tr key={index}>
                <th scope="row">{index+1}</th>
                <td>{item}</td>
                <td>
                  <Button color="primary" style={{marginRight: "20px"}} onClick={()=>{ handleEdit(index)}}>Edit</Button>
                  <Button color="danger" onClick={()=>{ handleDelete(index)}}>Delete</Button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>}
    </>
  )
}

export default CRUDWithHooks