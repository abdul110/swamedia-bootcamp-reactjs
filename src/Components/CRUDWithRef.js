import { useEffect, useRef, useState } from "react"
import axios from "axios"

const CRUDWithRef = ()=>{
  const formRef = useRef()
  const [input, setInput] = useState({})
  const [contestants, setContestants] = useState(null)
  const [showForm, setShowForm] = useState(false)
  const [currentId, setCurrentId] = useState(null) // create state



  const fetchData = async ()=>{
    let result = await axios.get('https://super-bootcamp-backend.sanbersy.com/api/data-contestants')
    setContestants(result.data)
  }


  const handleSubmit = async (event)=>{
    event.preventDefault()
    const formElements = [...formRef.current.elements]

    formElements.forEach((element)=>{
      input[element.name] = element.type === 'number' ? Number(element.value) : element.value
    })
    

    if (currentId){
      await axios.put(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${currentId}`, input)
    }else{
      await axios.post('https://super-bootcamp-backend.sanbersy.com/api/data-contestants', input)
    }
    setInput({})
    setContestants(null)
    setShowForm(false)
    setCurrentId(null)
  }

  const backToTable = ()=>{
    setShowForm(false)
  }

  const handleEdit = async (id)=>{
    const result =  await axios.get(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${id}`)
    const {name, height, gender} = result.data

    setInput({name, height, gender})

    setCurrentId(id)
    setShowForm(true)
  }

  const handleDelete = async (id)=>{
    await axios.delete(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${id}`)

    setContestants(null)
    setCurrentId(null)
  }


  const addNewContestant = ()=>{
    setShowForm(true)
  }

  useEffect(()=>{
    if (contestants === null){
      fetchData()
    }
  },[contestants])
  return(
    <>
      { showForm ? 
        <div className="custom-form-section">
          <form ref={formRef} className="custom-form" onSubmit={handleSubmit}>
            <label htmlFor="name">Name</label>
            <input required type="text" placeholder="Contestant name.." defaultValue={input.name} name="name"/>
            <label htmlFor="gender">Gender</label>
            <select required placeholder="Contestant gender.." defaultValue={input.gender} name="gender">
              <option value="Laki-Laki">Laki Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
            <label htmlFor="height">Height</label>
            <input required type="number" min={1} max={250} defaultValue={input.height} placeholder="Contestant height.." name="height"/>      
            <input type="submit" value={currentId ? "Update": "Submit"}/>
            <button onClick={backToTable} className="custom-button button-primary">
              Back
            </button>
          </form>
        </div> :
        <div style={{textAlign: "left"}} >
          <button onClick={addNewContestant} className="custom-button button-success">
            Add New Contestant
          </button>
        </div>
      }

      {
       !showForm && (
          <table className="custom-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {contestants && contestants.map((item, index)=>{
                return (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>
                      <button onClick={()=>{ handleEdit(item.id)}} className="custom-button button-primary">Edit</button>
                      <button onClick={()=>{ handleDelete(item.id)}} className="custom-button button-danger">Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
     )}

    </>
  )
}

export default CRUDWithRef