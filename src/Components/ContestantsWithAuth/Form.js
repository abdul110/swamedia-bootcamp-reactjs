import axios from "axios"
import { useContext, useRef, useState } from "react"
import { ContestantsContext } from "../../Context/ContestantsContext"
import { UserContext } from "../../Context/UserContext"

const Form = ()=>{
  const {setContestants, showForm, setShowForm} = useContext(ContestantsContext)
  const [user] = useContext(UserContext)

  const formRef = useRef()
  const handleSubmit = async (event)=>{
    event.preventDefault()
    const input = {}
    const formElements = [...formRef.current.elements]

    formElements.forEach((element)=>{
      input[element.name] = element.type === 'number' ? Number(element.value) : element.value
    })
    try {
      await axios.post('https://super-bootcamp-backend.sanbersy.com/api/contestants', input, {headers: {"Authorization" : "Bearer "+ user.token}})
      setContestants(null)
      formElements.forEach((element)=>{
        element.value = ""
      })
      backToTable()
    }catch(err){
      alert(err)
    }
  }

  const backToTable = ()=>{
    setShowForm(false)
  }

  return(
    <>
      {showForm &&  <div className="custom-form-section">
        <form ref={formRef} className="custom-form" onSubmit={handleSubmit}>
          <label htmlFor="name">Name</label>
          <input required type="text" placeholder="Contestant name.."  name="name"/>
          <label htmlFor="gender">Gender</label>
          <select required placeholder="Contestant gender.." name="gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          <label htmlFor="height">Height</label>
          <input required type="number" min={1} max={250} placeholder="Contestant height.."  name="height"/>      
          <input type="submit" value="Submit"/>
          <button onClick={backToTable} className="custom-button button-primary">
            Back
          </button>
        </form>
      </div>}
    </>
  )
}

export default Form