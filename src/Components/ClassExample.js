import { Button } from "reactstrap"
import React, {Component} from "react"

class ClassExample extends Component{
  constructor(props){
    super(props)
    this.state= {
      count: 0,
      alert: false
    }
  }

  componentDidMount(){
    this.setState({count : this.props.start})
  }

  componentDidUpdate(){
    if (this.state.alert === false){
      if (this.state.count <= 0){
        this.setState({
          alert: true
        })
      }
    }else{
      if (this.state.count > 0){
        this.setState({
          alert: false
        })
      }
    }
  }

  plus = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  minus = () => {
    let count = this.state.count - 1
    this.setState({
      count
    })
  }

  render(){
    return (
      <>
        {this.state.alert && <h2>minimum number is zero</h2>}
        <h3>hitung {this.state.count}</h3>
        <Button outline onClick={this.plus} style={{marginRight: "20px"}}>+</Button>
        <Button outline onClick={this.minus} disabled={this.state.alert}>-</Button>
      </>
    )
  }

}

export default ClassExample