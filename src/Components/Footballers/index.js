import { ContestantsProvider } from "../../Context/ContestantsContext"
import Form from "./Form"
import Table from "./Table"

const Contestants = ()=>{
  return(
    <ContestantsProvider>
      <Form/>
      <Table/>
    </ContestantsProvider>
  )
}

export default Contestants