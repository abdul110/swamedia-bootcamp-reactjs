import { addDoc, collection, doc, getDoc, Timestamp, updateDoc } from "firebase/firestore"
import { useEffect, useRef, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { db, storage } from "../../firebase"
import { getDownloadURL, ref, uploadBytes } from "firebase/storage"

const Form = ()=>{
  let { id } = useParams();
  const [input,setInput] = useState({})
  const formRef = useRef()
  let navigate = useNavigate();
  const handleSubmit = async (event)=>{
    event.preventDefault()
    const formElements = [...formRef.current.elements]

    let photo;
    formElements.forEach((element)=>{
      if (element.name.length > 0){
        if (element.type === "file"){
          photo = element.files[0]
        }else{
          input[element.name] = element.type === 'number' ? Number(element.value) : element.value
        }
      }
    })

    if (photo){
      const storageRef = ref(storage, `photos/${photo.name}`);
  
      await uploadBytes(storageRef, photo)
      let imageUrl = await getDownloadURL(storageRef)
      input.image_url= imageUrl
    }

    try{
      if (id){
        const footballerRef = doc(db, "footballers", id);

        await updateDoc(footballerRef, {
          ...input,
          updated_at: Timestamp.now()
        });
      }else{
        await addDoc(collection(db, "footballers"),{
          ...input,
          created_at: Timestamp.now(), 
          updated_at: Timestamp.now(), 
        });
      }
      backToTable()
    }catch(err){
      alert(err)
    }
  }

  const backToTable = ()=>{
    navigate('/footballers')
  }

  useEffect(()=>{
    if (id){
      const fetchData = async ()=>{
        const docRef = doc(db, "footballers", id);
        const docSnap = await getDoc(docRef);
        setInput(docSnap.data())
      } 
      fetchData()
    }
  },[id])


  return(
    <>
      <div className="custom-form-section">
        <form ref={formRef} className="custom-form" onSubmit={handleSubmit}>
          <label htmlFor="name">Name</label>
          <input required type="text" placeholder="Footballer name.." defaultValue={input.name}  name="name"/>
          <label htmlFor="height">Height</label>
          <input required type="number" min={1} max={250} placeholder="Footballer height.."  defaultValue={input.height} name="height"/>
          <label htmlFor="age">Age</label>
          <input required type="number" min={1} max={250} placeholder="Footballer age.."  defaultValue={input.age} name="age"/>     
          <label htmlFor="file">File</label>
          <input type="file" placeholder="Footballer height.." defaultValue={input.photo} name="photo"/>      
          <input type="submit" value="Submit"/>
          <button onClick={backToTable} className="custom-button button-primary">
            Back
          </button>
        </form>
      </div>
    </>
  )
}

export default Form