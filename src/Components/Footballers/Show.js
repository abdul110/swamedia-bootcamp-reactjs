import { doc, getDoc } from "firebase/firestore";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import notFoundImg from '../../image-not-found-icon.png';
import { db } from "../../firebase";
import { Card, CardBody, CardText, CardTitle, ListGroup, ListGroupItem } from "reactstrap";

const FootballerShow = ()=>{
  let { id } = useParams();
  let navigate = useNavigate();
  const [footballer, setFootballer] = useState({})

  useEffect(()=>{
    if (id){
      const fetchData = async ()=>{
        const docRef = doc(db, "footballers", id);
        const docSnap = await getDoc(docRef);
        setFootballer(docSnap.data())
      } 
      fetchData()
    }
  },[id])

  const backToTable = ()=>{
    navigate('/footballers')
  }



  return (
    <>
      <Card
        style={{
          width: '18rem',
          margin: "auto"
        }}
      >
        <img
          alt="Card"
          src={footballer.image_url ? footballer.image_url : notFoundImg}
        />
        <CardBody>
          <CardTitle tag="h5">
            {footballer.name}
          </CardTitle>
        </CardBody>
        <ListGroup flush>
          <ListGroupItem>
            Age: {footballer.age} years old
          </ListGroupItem>
          <ListGroupItem>
            Height: {footballer.height} cm
          </ListGroupItem>
        </ListGroup>
      </Card>

      <button onClick={backToTable} className="custom-button button-primary">
        Back
      </button>
    </>
  )
}

export default FootballerShow