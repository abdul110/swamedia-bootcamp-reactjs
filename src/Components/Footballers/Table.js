import { collection, deleteDoc, doc, getDocs } from "firebase/firestore";
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import Swal from "sweetalert2";
import { db } from "../../firebase";

const Table = ()=>{
  let navigate = useNavigate();
  const [footballers, setFootballers] = useState(null)


  const fetchData = async ()=>{

    const result = await getDocs(collection(db, "footballers"));
    
    let items = []
    result.forEach((doc)=>{
      const data = doc.data()
      items = [...items, {id: doc.id, ...data}]
    })

    setFootballers(items)

  }

  const addNewFootballer = ()=>{
    navigate(`/footballers/create`)

  }

  const showFootballer = (id)=>{
    navigate(`/footballers/${id}`)
  }

  const editFootballer = (id)=>{
    navigate(`/footballers/${id}/edit`)
  }

  const deleteFootballer = (id)=>{
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async (result) => {
      if (result.isConfirmed) {
        await deleteDoc(doc(db, "footballers", id));

        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )

        setFootballers(null)
      }
    })

  }


  useEffect(()=>{
    if (footballers === null){
      fetchData()
    }
  },[footballers])

  return(
    <>
        <div style={{textAlign: "left"}} >
          <button onClick={addNewFootballer} className="custom-button button-success">
            Add New Footballer
          </button>
        </div>
        <table className="custom-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Height</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {footballers && footballers.map((item, index)=>{


              return (
                <tr key={index}>
                  <td>{index+1}</td>
                  <td>{item.name}</td>
                  <td>{item.height}</td>
                  <td>
                    <button onClick={()=>{showFootballer(item.id)}} className="custom-button button-success">
                      Show
                    </button>
                    &nbsp;
                    <button onClick={()=>{editFootballer(item.id)}} className="custom-button button-primary">
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={()=>{deleteFootballer(item.id)}} className="custom-button button-danger">
                      Delete
                    </button>
                  </td>
                  {/* <td>{item.height}</td>
                  <td>{item.gender}</td> */}
                </tr>
              )
            })}
          </tbody>
        </table>
    </>
  )
}

export default Table