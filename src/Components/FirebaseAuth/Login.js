import { signInWithEmailAndPassword } from "firebase/auth"
import { useContext, useRef, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Button, Card, CardBody, FormGroup, Input, Label, Form } from "reactstrap"
import Swal from "sweetalert2"
import { UserContext } from "../../Context/UserContext"
import { auth } from "../../firebase"

const Login = ()=>{
  let navigate = useNavigate()
  const [input, setInput] = useState({email: "", password: ""})
  const [, setUser] = useContext(UserContext)

  const loginHandler = async (event)=>{

    event.preventDefault()
    
    signInWithEmailAndPassword(auth, input.email, input.password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;
      const currentUser = {email: user.email, authType: "firebase"}
      setUser(currentUser)
      localStorage.setItem("user", JSON.stringify(currentUser))
    })
    .catch((error) => {
      Swal.fire({
        icon: 'warning',
        title: 'Oops...',
        text: 'invalid email or password',
      })

    });
  }

  const handleChange = (event)=>{
    const {name, value} = event.target
    setInput({...input, [name]: value})
  }

  return (
    <Card style={{textAlign: "left", width: "40%", margin: "auto"}}>
      <CardBody>
        <h1>Login</h1>
        <Form onSubmit={loginHandler}>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="email" className="mr-sm-2">
              Email
            </Label>
            <Input
              type="email"
              name="email"
              onChange={handleChange}
              value={input.email}
              id="email"
              placeholder="email"
            />
          </FormGroup>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="password" className="mr-sm-2">
              Password
            </Label>
            <Input
              type="password"
              name="password"
              onChange={handleChange}
              value={input.password}
              id="password"
              placeholder="password"
            />
          </FormGroup>
          <Button type="submit" color="primary">
            Login With Firebase
          </Button>
          <br/>
          <strong>or</strong>
          <br/>
          <Button onClick={()=> navigate("/login")} color="success">
            Login without Firebase
          </Button>
        </Form>
      </CardBody>
    </Card>
  )
}

export default Login