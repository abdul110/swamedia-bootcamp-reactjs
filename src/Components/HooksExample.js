import { useEffect, useState } from "react"
import { Button } from "reactstrap"

const HooksExample = ()=>{
  const [count, setCount] = useState(1)
  const [disable, setDisable] = useState(1)

  const timesTwo = ()=>{
    setCount(count * 2)
  }
  
  const divideTwo = ()=>{
    setCount(count / 2)
  }

  useEffect(()=>{
    if (count <= 1){
      setDisable(true)
    }else{
      setDisable(false)
    }
  },[count])

  return (
    <>
      <h3>{count}</h3>
      <Button color="primary" onClick={timesTwo} style={{marginRight: "20px"}}>x 2</Button>
      <Button color="primary" onClick={divideTwo} disabled={disable} >/ 2</Button>
    </>
  )
}

export default HooksExample