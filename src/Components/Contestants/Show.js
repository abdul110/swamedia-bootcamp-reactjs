import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ContestantShow = ()=>{
  let { id } = useParams();
  const [contestant, setContestant] = useState({})

  const prefix = (gender)=>{
    if (gender === "Male"){
      return "Mr"
    }else if(gender === "Female"){
      return "Mrs"
    }else{
      return ""
    }
  }


  useEffect(()=>{
    if (id){
      axios.get(`https://super-bootcamp-backend.sanbersy.com/api/data-contestants/${id}`).then((res)=>{
        setContestant(res.data)
      }).catch((err)=>{console.log(err)})
    }
  },[id])



  return (
    <>
      <h1>Name: {prefix(contestant.gender)} {contestant.name}</h1>
      <h1>Height: {contestant.height} cm</h1>
    </>
  )
}

export default ContestantShow