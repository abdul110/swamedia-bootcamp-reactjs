import axios from "axios"
import { useContext, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { ContestantsContext } from "../../Context/ContestantsContext"

const Table = ()=>{
  let navigate = useNavigate();

  const {contestants, setContestants, showForm, setShowForm} = useContext(ContestantsContext)

  const fetchData = async ()=>{
    let result = await axios.get('https://super-bootcamp-backend.sanbersy.com/api/data-contestants')
    setContestants(result.data)
  }

  const addNewContestant = ()=>{
    setShowForm(true)
  }

  const showContestant = (id)=>{
    navigate(`/contestants/${id}`)
  }

  useEffect(()=>{
    if (contestants === null){
      fetchData()
    }
  },[contestants])

  return(
    <>
      {!showForm && 
        <>
          <div style={{textAlign: "left"}} >
            <button onClick={addNewContestant} className="custom-button button-success">
              Add New Contestant
            </button>
          </div>
          <table className="custom-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
                {/* <th>Height</th>
                <th>Gender</th> */}
              </tr>
            </thead>
            <tbody>
              {contestants && contestants.map((item, index)=>{
                return (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>
                      <button onClick={()=>{showContestant(item.id)}} className="custom-button button-success">
                        Show
                      </button>
                    </td>
                    {/* <td>{item.height}</td>
                    <td>{item.gender}</td> */}
                  </tr>
                )
              })}
            </tbody>
          </table>
        </>
      }
    </>
  )
}

export default Table