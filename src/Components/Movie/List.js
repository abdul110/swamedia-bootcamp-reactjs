import { useContext } from "react"
import { MoviesContext } from "../../Context/MoviesContext"

const List = ()=>{
  const [movies] = useContext(MoviesContext)

  return(
    <div style={{textAlign: "left"}}>
      <ol>
        {movies.map((movie)=>{
          return (<li>{movie.name} {movie.durationInMinutes} minutes</li>)
        })}
      </ol>
    </div>
  )
}

export default List