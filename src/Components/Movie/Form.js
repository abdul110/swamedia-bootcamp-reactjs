import { useContext, useRef } from "react"
import { MoviesContext } from "../../Context/MoviesContext"

const Form = ()=>{
  const [movies, setMovies] = useContext(MoviesContext)
  const inputRef = useRef()

  const onSubmit = (event)=>{
    event.preventDefault()
    const name = inputRef.current.value
    setMovies([...movies, {name}])
    inputRef.current.value = ""
  }

  return(
    <>
      <form onSubmit={onSubmit}>
        <input type="text" name="title" ref={inputRef} required/>
        <input type="submit" value="Submit"/>
      </form>
    </>
  )
}

export default Form