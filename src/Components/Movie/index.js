import { MoviesProvider } from "../../Context/MoviesContext"
import Form from "./Form"
import List from "./List"

const Movie = ()=>{
  return(
    <MoviesProvider>
      <Form/>
      <List/>
    </MoviesProvider>
  )
}

export default Movie