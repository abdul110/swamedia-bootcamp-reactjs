import React, {Component} from "react"
import logo from '../logo.svg';

class Content extends Component{
  render(){
    let numbers = []

    for (let i=1; i<=this.props.times; i++){
      numbers = [...numbers, i]
    }

    console.log(numbers)
    return(
      <>
        <header className="App-header">
          {numbers.map((number)=>{
            return(
              <>
                {number}
                <img src={logo} className="App-logo" alt="logo" />
              </>
            )
          })}
        </header>
      </>
    )
  }
}

export default Content