import axios from "axios"
import { useContext, useRef, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Button, Card, CardBody, FormGroup, Input, Label, Form } from "reactstrap"
import { UserContext } from "../../Context/UserContext"

const Register = ()=>{
  let navigate = useNavigate();
  const [input, setInput] = useState({name: "", email: "", password: ""})
  const [, setUser] = useContext(UserContext)

  const registerHandler = async (event)=>{
    event.preventDefault()
    
    const response = await axios.post("https://super-bootcamp-backend.sanbersy.com/api/register", input)

    const {user, token} = response?.data
    const currentUser = {name: user.name, email: user.email, token}
    setUser(currentUser)
    localStorage.setItem("user", JSON.stringify(currentUser))
  }

  const handleChange = (event)=>{
    const {name, value} = event.target
    setInput({...input, [name]: value})
  }

  return (
    <Card style={{textAlign: "left", width: "40%", margin: "auto"}}>
      <CardBody>
        <h1>Register</h1>
        <Form onSubmit={registerHandler}>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="name" className="mr-sm-2">
              Username
            </Label>
            <Input
              type="text"
              name="name"
              onChange={handleChange}
              value={input.name}
              id="name"
              placeholder="username"
            />
          </FormGroup>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="email" className="mr-sm-2">
              Email
            </Label>
            <Input
              type="email"
              name="email"
              onChange={handleChange}
              value={input.email}
              id="email"
              placeholder="email"
            />
          </FormGroup>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="password" className="mr-sm-2">
              Password
            </Label>
            <Input
              type="password"
              name="password"
              onChange={handleChange}
              value={input.password}
              id="password"
              placeholder="password"
            />
          </FormGroup>
          <Button type="submit" color="primary">
            Register
          </Button>
          <br/>
          <strong>or</strong>
          <br/>
          <Button onClick={()=> navigate("/firebase-register")} color="success">
            Register with Firebase
          </Button>
        </Form>
      </CardBody>
    </Card>
  )
}

export default Register