import axios from "axios"
import { useContext, useRef, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Button, Card, CardBody, FormGroup, Input, Label, Form } from "reactstrap"
import { UserContext } from "../../Context/UserContext"

const Login = ()=>{
  let navigate = useNavigate()
  const [input, setInput] = useState({email: "", password: ""})
  const [, setUser] = useContext(UserContext)

  const loginHandler = async (event)=>{
    event.preventDefault()
    
    const response = await axios.post("https://super-bootcamp-backend.sanbersy.com/api/login", input)

    const {user, token} = response?.data
    const currentUser = {name: user.name, email: user.email, token}
    setUser(currentUser)
    localStorage.setItem("user", JSON.stringify(currentUser))
  }

  const handleChange = (event)=>{
    const {name, value} = event.target
    setInput({...input, [name]: value})
  }

  return (
    <Card style={{textAlign: "left", width: "40%", margin: "auto"}}>
      <CardBody>
        <h1>Login</h1>
        <Form onSubmit={loginHandler}>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="email" className="mr-sm-2">
              Email
            </Label>
            <Input
              type="email"
              name="email"
              onChange={handleChange}
              value={input.email}
              id="email"
              placeholder="email"
            />
          </FormGroup>
          <FormGroup className="pb-2 mr-sm-2 mb-sm-0">
            <Label for="password" className="mr-sm-2">
              Password
            </Label>
            <Input
              type="password"
              name="password"
              onChange={handleChange}
              value={input.password}
              id="password"
              placeholder="password"
            />
          </FormGroup>
          <Button type="submit" color="primary">
            Login
          </Button>
          <br/>
          <strong>or</strong>
          <br/>
          <Button onClick={()=> navigate("/firebase-login")} color="success">
            Login with Firebase
          </Button>
        </Form>
      </CardBody>
    </Card>
  )
}

export default Login