import { useNavigate } from 'react-router-dom';
import { Button } from 'reactstrap';

const Home = ()=>{
  let navigate = useNavigate();

  const goToContentExample = ()=>{
    navigate('/content-example')
  }

  return <Button color="primary" onClick={goToContentExample}>go To Content Example</Button>;
}

export default Home